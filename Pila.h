#include <iostream>
#include <list>
using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila {
  private:
    int tope = 0;
    int max = 0;
    bool band = " ";
    int dato = 0;
    list<Pila> pila;

  public:
    Pila();
    Pila(int tope, int max);

    int get_tope();
    int get_max();
    bool get_band();
    list<Pila> get_pila();
    void set_tope(int tope);
    void set_max(int max);
    void set_bool(bool band);
    void add_pila(Pila pila);
    void pila_vacia(int tope, bool band);
    void pila_llena(int tope, int max, bool band);
    void push(int tope, int max, bool band);
    void pop(int tope);
};
#endif
