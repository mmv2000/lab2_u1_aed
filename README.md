# Laboratorio pilas
Se solicita escribir un programa que implemente los algoritmos para manipular y mostrar una pila.
Manipular la pila indica que se pueden agregar elementos (push) y eliminar elementos(pop).

# Como funciona
El programa funciona mediante un menu principal, el cual muestra las opciones correspondientes a realizar
en el programa siendo 1 para agregar elemento, 2 para eliminar, 3 para mostrar la pila y 0 para salir.
Mediante el uso de funciones como lo son push(), pop() y mostrar_pila(), desarrollaremos la interacción con
el objeto pila, añadiendo, eliminando y mostrando los cambios generados por el programa.

# Para ejecutar
Se debe abrir la terminal e ingresar al directorio donde se guardó el ejercicio con sus archivos .cpp y .h
repectivamente. Luego ejecutar el comando make para su compilación (se recomienda hacer un make clean para
iniciar el programa sin ningún contratiempo y luego ejecutar el comando ./pila para iniciar el programa.

# Construido con 

- Ubuntu: sistema operativo

- C++: lenguaje de programación

- iostream: libreria

- Atom: editor de texto

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.46.0

# Autor

- Martín Muñoz Vera 
