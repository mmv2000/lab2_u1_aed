/*
  * g++ Cuadrado.cpp -o Cuadrado
 */

#include <iostream>
#include <list>
using namespace std;
#include "Pila.h"

void menu() {
  cout << "MENU DE OPCIONES: " << endl;
  cout << " PRESIONE: 1 PARA AGREGAR/PUSH ELEMENTO" << endl;
  cout << " PRESIONE: 2 PARA REMOVER/POP" << endl;
  cout << " PRESIONE: 3 PARA VER PILA" << endl;
  cout << " PRESIONE: 0 PARA SALIR" << endl;
}


void push(int pila[], int tope, int max, int &dato, bool &band) {
  pila_llena(tope, max, band);
  if(band == false){
    tope = tope + 1;
    pila[tope] = dato;
    cout << pila[tope] << endl;
  }
  else{
    cout << "Desbordamiento, pila llena" << endl;
  }
}

int pop (Pila pila, int tope) {
  pila_vacia(tope, band);
    if (tope <= -1){
    cout << "Subdesbordamiento, Pila vacı́a"<< endl;
  }
  else{
    cout << "Elemento eliminado: " << pila[tope] << endl;
    tope --;
  }
}


void mostrar_pila(Pila pila[], int tope) {
  if(tope >= 0){
    for(int i = tope; i >= -1; i--){
      cout << pila[i] <<endl;
    }
  }
  else{
    cout << "Pila vacia" << endl;
  }
}

int main(int argc, char const *argv[]) {
  int opcion;
  int dato;
  int elimina;
  int max = 5;
  int tope = -1;
  bool band;
  Pila pila[max];

  do {
    menu();
    cin >> opcion;
    switch (opcion) {
      case 1:
        cout << "dato que desea agregar" << endl;
        cin >> dato;
        push(pila, tope, max, dato, band);
        cout << "dato apilado: " << dato << endl;
      break;

      case 2:
        eliminar = pop(pila, tope);
        cout << "dato desapilado:" << eliminar << endl;
      break;

      case 3:
        cout << "PILA: " << endl;
        mostrar_pila(pila);
      break;
  }
} while(opcion != 0);

  return 0;
}
